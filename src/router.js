import Vue from "vue";
import VueRouter from 'vue-router'
import Home from "@/components/pages/Home";
import Todo from "@/components/pages/Todo";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        redirect: '/todos',
        name: 'home'
    },
    {
        path: '/todos',
        name: 'todos',
        component: Home
    },
    {
        path: '/todo',
        name: 'todo',
        component: Todo
    }
]
const router = new VueRouter({
    mode: 'history',
    routes: routes
})

export default router